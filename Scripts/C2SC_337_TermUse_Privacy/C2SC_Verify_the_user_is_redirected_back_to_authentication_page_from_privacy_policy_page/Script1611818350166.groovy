import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

Mobile.startApplication(GlobalVariable.sAppID, true)

//Mobile.waitForElementPresent(findTestObject('null'), 60)
//Mobile.waitForElementPresent(findTestObject('Object Repository/C2SC_Common_Object/tab_home'), GlobalVariable.intWaitTime)
Mobile.tap(findTestObject('Object Repository/C2SC_Common_Object/btn_NextArrow'), GlobalVariable.intWaitTime)

Mobile.waitForElementPresent(findTestObject('C2SC_337_TermUse_Privacy/lnk_PrivacyPolicy'), GlobalVariable.intWaitTime)

Mobile.tap(findTestObject('C2SC_337_TermUse_Privacy/lnk_PrivacyPolicy'), GlobalVariable.intWaitTime)

Mobile.waitForElementPresent(findTestObject('C2SC_337_TermUse_Privacy/txt_PrivacyNotice'), GlobalVariable.intWaitTime)

Mobile.getText(findTestObject('C2SC_337_TermUse_Privacy/txt_PrivacyNotice'), GlobalVariable.intWaitTime)

Mobile.tap(findTestObject('C2SC_Common_Object/btn_Back'), GlobalVariable.intWaitTime)

Mobile.waitForElementPresent(findTestObject('C2SC_337_TermUse_Privacy/lnk_PrivacyPolicy'), GlobalVariable.intWaitTime)

Mobile.verifyElementVisible(findTestObject('C2SC_337_TermUse_Privacy/lnk_PrivacyPolicy'), GlobalVariable.intWaitTime)

Mobile.closeApplication()

