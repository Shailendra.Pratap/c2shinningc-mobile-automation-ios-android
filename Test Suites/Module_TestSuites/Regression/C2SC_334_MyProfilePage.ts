<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>C2SC_334_MyProfilePage</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>31b8e6c8-410e-46d6-a724-87f029d84bc5</testSuiteGuid>
   <testCaseLink>
      <guid>542abd5a-cc86-4efb-93f4-7ef2d2f23080</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_334_MyProfilePage/C2SC_Test_to_check_the_details_shown_in_the_my_profile_page</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3a0441df-00c8-4f9b-bc03-8db9231f8cbf</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_334_MyProfilePage/C2SC_Test_to_check_the_navigation_from_my_profile_page_to_home</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
