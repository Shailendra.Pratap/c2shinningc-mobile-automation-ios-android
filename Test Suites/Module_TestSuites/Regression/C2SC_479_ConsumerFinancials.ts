<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>C2SC_479_ConsumerFinancials</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>c0d3f534-ccde-45cd-9204-65b9fedeff34</testSuiteGuid>
   <testCaseLink>
      <guid>93fedcd4-75e9-49ab-a426-20d918be13cf</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_479_ConsumerFinancials/C2SC_Verify_that_buyer_able_to_view_the_details_of_the_property_and_UX_design</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>311efdbc-0c22-4249-aada-4505cbda66df</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_479_ConsumerFinancials/C2SC_Verify_that_buyer_able_to_view_the_Down_payment_of_the_property</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>675a8c63-f278-474c-ba33-d8aa0188750c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_479_ConsumerFinancials/C2SC_Verify_that_buyer_able_to_view_the_estimated_closing_costs_of_the_property</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ee578761-8c79-422a-b13a-fb69e1089e0f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_479_ConsumerFinancials/C2SC_Verify_that_buyer_able_to_view_the_estimated_monthly_cost_of_the_property</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d175fe75-d1fa-46d3-b75f-8b621924ff20</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_479_ConsumerFinancials/C2SC_Verify_that_buyer_able_to_view_the_list_price_of_the_property</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>889ad649-c9a9-4bcc-93a3-efb3e5c5025d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_479_ConsumerFinancials/C2SC_Verify_that_buyer_able_to_view_the_mortgage_rate_along_with_the_interest</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
